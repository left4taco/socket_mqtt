import numpy as np

for j in np.arange(4):
    for i in np.arange(7):
        print("(({i1}, {j}, 1), ({i2}, {j}, 1)),".format(
            i1=i,
            i2=i+1,
            j=j
        ))

for i in np.arange(8):
    for j in np.arange(3):
        print("(({i}, {j1}, 1), ({i}, {j2}, 1)),".format(
            j1=j,
            j2=j+1,
            i=i
        ))
