## Description

This package contains implementations for online-plan synthesis algorithm give a dynamic
environment (as the finite transition system) and a potential infeasible Linear temporal
logic formula (as the robot’s task). It outputs the predicted trajectories at each
time-step that optimize the reward-related objectives and finally fulfill the task maximumly.

![grid.png](grid.png)

## Reference

Receding Horizon Control Based Online LTL Motion Planning in Partially Infeasible
Environments. Meng Guo and Dimos V. Dimarogonas. Journal of Autonomous Robot.

[Experiment video](https://www.youtube.com/watch?v=16j6TmVUrTk)

## Features

- Allow both normal and infeasible LTL based product automaton task formulas
- Motion model can be dynamic and initially unknown
- Soft specification is maximumly satisfied.
- Online-Path planning is designed from the model predicted control methodology.
- Collect and transfer the real-time data via Optitrack camera systems 
- Allow automatically calibrate the mobile robots to obtain its orientation and dynamics.

